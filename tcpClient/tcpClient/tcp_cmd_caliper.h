#pragma once

#include "tcp_cmd_common.h"
#define CALIPER_HEADER_PAGE_SIZE	512			// 1 Page = 512 bytes
#define CALIPER_HEADER_TOTAL_PAGE	20			// total page using = 20 pages for a header,10240 Byte header

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define MaxNArms 		22
#define MaxNOdometer 	3
#define	MaxExtParams	10

typedef enum
{
	// ======================================================================
	// page 0 : OperatingMode, CaliperInfo
	UnknowCaliperCommand									= 0 * 16 + 0,		
	UnknowCaliperResponse									= 0 * 16 + 1,		

	SetCaliperModeCommand									= 0 * 16 + 2,		
	SetCaliperModeResponse									= 0 * 16 + 3,		

	GetCaliperModeCommand									= 0 * 16 + 4,		
	GetCaliperModeResponse									= 0 * 16 + 5,		

	SetCaliperInfoCommand									= 0 * 16 + 6,		
	SetCaliperInfoResponse									= 0 * 16 + 7, 		

	GetCaliperInfoCommand									= 0 * 16 + 8,		
	GetCaliperInfoResponse									= 0 * 16 + 9,

	SetCaliperStartStopRecordCommand						= 0 * 16 + 10,
	SetCaliperStartStopRecordResponse						= 0 * 16 + 11,
	
	SetCaliperBankPageCommand								= 0 * 16 + 12,
	SetCaliperBankPageResponse								= 0 * 16 + 13,

	GetCaliperBankPageCommand								= 0 * 16 + 14,
	GetCaliperBankPageResponse								= 0 * 16 + 15,


	// ======================================================================
	// Page 1 : CalibrationParams
	SetCaliperCalibrationParamsCommand						= 1 * 16 + 0,		
	SetCaliperCalibrationParamsResponse						= 1 * 16 + 1,		

	GetCaliperCalibrationParamsCommand						= 1 * 16 + 2,		
	GetCaliperCalibrationParamsResponse						= 1 * 16 + 3,		

	// ======================================================================
	// Page 2 :  A-Scan
	SetCaliperAScanInfoCommand								= 2 * 16 + 0,		
	SetCaliperAScanInfoResponse								= 2 * 16 + 1,		

	GetCaliperAScanInfoCommand								= 2 * 16 + 2,		
	GetCaliperAScanInfoResponse								= 2 * 16 + 3,		

	GetCaliperAScanCommand									= 2 * 16 + 4,		
	GetCaliperAScanResponse									= 2 * 16 + 5,		

	// ======================================================================
	// Page 3 : B-Scan

	SetCaliperBScanInfoCommand								= 3 * 16 + 0,		
	SetCaliperBScanInfoResponse								= 3 * 16 + 1,		

	GetCaliperBScanInfoCommand								= 3 * 16 + 2,		
	GetCaliperBScanInfoResponse								= 3 * 16 + 3,		

	GetCaliperBScanCommand									= 3 * 16 + 4,		
	GetCaliperBScanResponse									= 3 * 16 + 5,

	// ======================================================================
	// Page 15 : Misc.
	GetCaliperSampleDataCommand								= 15 * 16 + 14,
	GetCaliperSampleDataResponse							= 15 * 16 + 15,	

}tcpCommandCaliper_t;

#pragma pack(push)			// push current alignment to stack //
#pragma pack(1)				// set alignment to 1 byte boundary //

typedef enum
{
	INVALID_RES = 0,
	SUCCESS_RES,
}res_code_t;

/***********************************NOT USED***********************************************************************************
typedef struct tagCaliperInfo
{
	TCP_PACKET_INFO 		PacketInfo;							///<TCP Packet info header 3 bytes
	uint8_t					nArms;								///<The number of boards connecting to G25
	uint8_t					nOdometer; 							///<The number of odometer channels
	uint8_t					nOdometerType;						///<Odometer type : pulse, magnet
	float					OdometerDiameterInMM;				///<Diameter of odometer wheel in mm unit
	uint8_t					OdometerPulsePerRevolution;			///<The number of pulses per revolution (for pulse odometer)
	char					LinuxFirmwareVersion[12];			///<Linux firmware, i.e., 1.0.0	
	float					InternalPipeDiameterInMM;			///<Internal Pipe Diameter in MM
	float					BodyDiameterInMM;
	float					ArmLengthInMM;
	uint32_t				HeaderSizeInByte;					///<if 0 = 512 x 20 pages, otherwise 512 x Reserved page (default = 0 for IMU)
}CALIPER_INFO;


typedef struct tagCaliperAScanInfo
{
	TCP_PACKET_INFO			PacketInfo;				///<TCP Packet info header 3 bytes	
	uint8_t					ArmIndex;				///<		
	uint16_t				nSamplingRate_SPS;		///<SPS (Range: 1..2,000) for plotting
	uint16_t				nSamplePoint;			///<The number of sample points
	uint8_t 				isAScanReady; 			///<0 : no, 1 : yes
}CALIPER_ASCAN_INFO;

#define CALIPERSAMPLES 4000


typedef struct tagCaliperAScan
{
	TCP_PACKET_INFO			PacketInfo;	///<TCP Packet info header 3 bytes
	uint8_t					ArmIndex;						///<		
	uint16_t				nSamplingRate_SPS;				///<SPS (Range: 1..2,000) for plotting
	uint16_t				nSamplePoint;					///<The number of sample points
	unsigned short  		SensorValue[CALIPERSAMPLES];	///<Data pointer points to the first element of array
}CALIPER_ASCAN;

// Add for calibration 
typedef struct tagCaliperBScan
{
	TCP_PACKET_INFO			PacketInfo;						///<TCP Packet info header 3 bytes	
	unsigned short  		SensorValue[16];				///<Data pointer points to the first element of array
}CALIPER_BSCAN;
******************************************************************************************************************************/

// Time stamp caliper packet
typedef struct
{
	unsigned short 			Year;	//1970-01-01 UTC - 20XX-01-01 UTC
	unsigned char			Month;	//1970-01-01 UTC - 1970-12-01 UTC
	unsigned char			Day;	//1970-01-01 UTC - 1970-01-31 UTC //Leap years (max day: 28,29,30,31)
	unsigned char 			Hour;	//00:00:00 - 23:00:00
	unsigned char  			Minute;	//00:00:00 - 00:59:00
	unsigned char 			Second;	//00:00:00 - 00:00:59
}TIME_STAMP;

typedef struct tagTCP_PACKET_SUBINFO
{
	float					FirmwareVer;					// Firmware Version xxx.xxx (major.minor)
	TIME_STAMP				TimeStamp;						// Time stamp: years: months: days: hours: minutes: seconds
}TCP_PACKET_SUBINFO;

// Sub variables for extra parameters
typedef struct
{
	unsigned short 			CalExt[MaxNArms];
	float					DiaExt;
}CAL_EXTRA_PARAMS;

/*****************************************************************************************************************************/

// Caliper parameters information
typedef struct
{
	unsigned short			SamplingRate;					//Rate of data sampling
	float					BodyDiameter;					//Body diameter of pig
	float					OdoDiameter;					//Odometer diameter
	float					CaliperArmLength;				//Caliper arm length
	float					CaliperArmCenterOffset;			//Caliper arm offset from body diameter
	unsigned char			OdoNumber;						//Odometer number
	unsigned short			ArmNumber;						//Arm number
}CALIPER_PARAMS_INFO;

typedef struct
{
	unsigned short			ArmsData[MaxNArms];				//Angle arms data (raw)
	int						OdoData[MaxNOdometer];			//Odometer data (raw)
}CALIPER_BSCAN_DATA;

typedef struct
{
	unsigned short			CalMaskBits;					//Mask bits of caliper calibration is used
	unsigned short 			CalParamMax[MaxNArms];			//Caliper array arms data (MAX)
	unsigned short 			CalParamMin[MaxNArms];			//Caliper array arms data (MIN)
	float					DiaMax;							//Diameter (MAX)
	float					DiaMin;							//Diameter (MIN)
	CAL_EXTRA_PARAMS		ParamsExt1;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt2;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt3;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt4;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt5;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt6;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt7;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt8;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt9;						//Extra parameters for calibration
	CAL_EXTRA_PARAMS		ParamsExt10;					//Extra parameters for calibration
}CALIPER_CAL_PARAMS;

////////////////////////
// TCP/IP data Packet //
////////////////////////
typedef struct tagTCP_PACKET_CAL
{
	TCP_PACKET_INFO 		PacketInfo;						// <TCP Packet info header 3 bytes
	TCP_PACKET_SUBINFO		PacketSubInfo;					// TCP Packet sub information
	union
	{
	 uint8_t					Data[maxDataLEN-sizeof(TCP_PACKET_SUBINFO)];			// <TCP Packet data
	 CALIPER_PARAMS_INFO		caliper_params_info;
	 CALIPER_BSCAN_DATA			caliper_bscan;
	 CALIPER_CAL_PARAMS			caliper_params_cal;
	}AppData;
}TCP_PACKET_CAL;

#pragma pack(pop)
