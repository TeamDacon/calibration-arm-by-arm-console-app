#pragma once

#ifdef _isSTM32_
#define maxDataLEN 1024
#else
#define maxDataLEN 20000
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


/***************************************************************************************\
\***************************************************************************************/
typedef enum
{
	SuspendingMode = 0,
	InspectingMode = 1,
	CalibratingMode = 2,
}OperatingMode_t;


/***************************************************************************************\
\***************************************************************************************/


////////////////////////////////////////////////////////
// Declares an enumeration data type called OdoType_t //
////////////////////////////////////////////////////////
typedef enum
{
	unknownOdo = 0,		///<unknown type
	Magnetic = 1,		///<Magentic Encoder = 1
	Pulse = 2,			///<Pulse = 2
}OdoType_t;


/***************************************************************************************\
\***************************************************************************************/

#pragma pack(push)			// push current alignment to stack //
#pragma pack(1)				// set alignment to 1 byte boundary //

////////////////////////
// TCP/IP Packet info //
////////////////////////
typedef struct tagTCP_PACKET_INFO
{
	uint8_t					PacketID;						// <PacketID 1 byte
	uint16_t				DataLength;						// <Number of valid bytes in the data section 2 bytes
}TCP_PACKET_INFO;


////////////////////////
// TCP/IP data Packet //
////////////////////////
typedef struct tagTCP_PACKET
{
	TCP_PACKET_INFO 		PacketInfo;						// <TCP Packet info header 3 bytes
	uint8_t					Data[maxDataLEN];				// <TCP Packet data
}TCP_PACKET;

typedef struct tagOperatingMode
{
	TCP_PACKET_INFO			PacketInfo;		///<TCP Packet info header 3 bytes
	uint8_t					OperatingMode;	///<0 : suspense, 1 : inspection mode, 2 : calibration \ref \see OperatingMode_t
	uint8_t					isSimulated; 	///< If OperationMode =CalibrationMode, fake two waveform, otherwise, read ADC data
}OPERATING_MODE;

#pragma pack(pop)
