// tcpClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "tcp_cmd_caliper.h"
#include "tcp_cmd_common.h"
#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <typeinfo>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "Mswsock.lib")
#pragma comment(lib, "AdvApi32.lib")

#define DEFAULT_BUFLEN 4096 * 8
#define DEFAULT_PORT "1500"
#define MAX_MSG 10000
#define MaxNArms 22


// int _tmain(int argc, _TCHAR* argv[])
int __cdecl main(int argc, char** argv)
{
    TCP_PACKET_CAL SendPacket;
    TCP_PACKET_CAL RecvPacket;
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    size_t data_length = 0;
    static int rcv_ptr = 0;
    static char rcv_msg[MAX_MSG];
    static int ret;
    char UserResp = 'n';
    struct addrinfo *result = NULL, *ptr = NULL, hints;
    char recvbuf[DEFAULT_BUFLEN];
    char buffRecv[DEFAULT_BUFLEN];
    int iResult;
    int scanned;
    int recvbuflen = DEFAULT_BUFLEN;
    unsigned short SamplingRate, ArmNB;
    BYTE OdometersNB;
    float ArmLength, OdoDia, BodyDia, ArmCenterOffset, MaxDia, MinDia, ETX1Dia;
    int i, j;
    unsigned short CalDataArmMax[MaxNArms];
    unsigned short CalDataArmEXT[10][MaxNArms];
    unsigned short CalDataArmMin[MaxNArms];

    // Validate the parameters
    if (argc != 2) {
        printf("usage: %s server-name\n", argv[0]);
        return 1;
    }

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    // Attempt to connect to an address until one succeeds
    for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

	// Initialize the data with 0s
	for (i = 0; i < MaxNArms; i++) {
		for (j = 0; j < 10; j++) {
			CalDataArmEXT[j][i] = 0;
		}
	}

    // GetCaliperInfoCommand
    SendPacket.PacketInfo.PacketID = GetCaliperInfoCommand;
    data_length += sizeof(SendPacket.PacketInfo);
    iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
    memset(buffRecv, 0x0, DEFAULT_BUFLEN); /* init buffer */
    // Receive until the peer closes the connection
    memset(recvbuf, 0x0, DEFAULT_BUFLEN); /* init buffer */
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    memcpy((void*)&RecvPacket, recvbuf, iResult);
    if (RecvPacket.PacketInfo.PacketID != GetCaliperInfoResponse)
        printf("\nGetCaliperInfo error\n");
    printf("\n");


    // SetCaliperInfoCommand
    memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
    SendPacket.PacketInfo.PacketID = SetCaliperInfoCommand;
    data_length += sizeof(SendPacket.PacketInfo);

    // Setup Header Sub-Info
    SendPacket.PacketSubInfo.FirmwareVer = RecvPacket.PacketSubInfo.FirmwareVer;
    data_length += sizeof(SendPacket.PacketSubInfo);

    int SamplingRateInt, ArmNBInt, OdometersNBInt;
    // Build the Packet data to send
    printf("||\t=================================\n");
    printf("||\tCaliper parameters information\n||\t\n");
    printf("||\tPlease enter as follow :\n");
    printf("||\t=================================\n");
    printf("||\tSampling Rate (Hz) :\n");
	printf("||\t");
    scanned = scanf_s("%d", &SamplingRateInt);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (SamplingRateInt <= 0) || (SamplingRateInt >= 65535)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%d", &SamplingRateInt);
		scanf_s("%*[^\n]");
		getchar();
    }
    SamplingRate = (unsigned short)SamplingRateInt;
    SendPacket.AppData.caliper_params_info.SamplingRate = SamplingRate;
    printf("||\tBody Diameter (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &BodyDia);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (BodyDia <= 0)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%f", &BodyDia);
		scanf_s("%*[^\n]");
		getchar();
    }
    SendPacket.AppData.caliper_params_info.BodyDiameter = BodyDia;
    printf("||\tOdometer Diameter (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &OdoDia);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (OdoDia <= 0)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%f", &OdoDia);
		scanf_s("%*[^\n]");
		getchar();
    }
    SendPacket.AppData.caliper_params_info.OdoDiameter = OdoDia;
    printf("||\tArm Length (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &ArmLength);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (ArmLength <= 0)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%f", &ArmLength);
		scanf_s("%*[^\n]");
		getchar();
    }
    SendPacket.AppData.caliper_params_info.CaliperArmLength = ArmLength;
    printf("||\tCenter to Arm (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &ArmCenterOffset);
    scanf_s("%*[^\n]");
    getchar();
    while (scanned != 1) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%f", &ArmCenterOffset);
		scanf_s("%*[^\n]");
		getchar();
    }
    SendPacket.AppData.caliper_params_info.CaliperArmCenterOffset = ArmCenterOffset;
    printf("||\tNumber of Odometer :\n");
	printf("||\t");
    scanned = scanf_s("%d", &OdometersNBInt);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (OdometersNBInt < 0) || (OdometersNBInt > 3)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%d", &OdometersNBInt);
		scanf_s("%*[^\n]");
		getchar();
    }
    OdometersNB = (BYTE)OdometersNBInt;
    SendPacket.AppData.caliper_params_info.OdoNumber = OdometersNB;
    printf("||\tNumber of Arm :\n");
	printf("||\t");
    scanned = scanf_s("%d", &ArmNBInt);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (ArmNBInt < 0) || (ArmNBInt > MaxNArms)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%d", &ArmNBInt);
		scanf_s("%*[^\n]");
		getchar();
    }
    ArmNB = (unsigned short)ArmNBInt;
    SendPacket.AppData.caliper_params_info.ArmNumber = ArmNB;
    printf("||\tMax Diameter (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &MaxDia);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (MaxDia < 0)) {
        printf("||\tInvalid parameter\n");
		printf("||\t");
		scanned = scanf_s("%f", &MaxDia);
		scanf_s("%*[^\n]");
		getchar();
    }
    printf("||\tEXT1 Diameter (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &ETX1Dia);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (MaxDia < ETX1Dia)) {
        if (scanned != 1)
            printf("||\tInvalid parameter\n");
        else
            printf("||\tYour MID diameter is bigger than the MAX one\n");
        printf("||\tEXT1 Diameter (mm) :\n");
		printf("||\t");
        scanned = scanf_s("%f", &ETX1Dia);
        scanf_s("%*[^\n]");
        getchar();
    }
    printf("||\tMin Diameter (mm) :\n");
	printf("||\t");
    scanned = scanf_s("%f", &MinDia);
    scanf_s("%*[^\n]");
    getchar();
    while ((scanned != 1) || (MinDia > ETX1Dia)) {
        if (scanned != 1)
            printf("||\tInvalid parameter\n");
        else
            printf("||\tYour MIN diameter is bigger than the MID one\n");
        printf("||\tMIN Diameter (mm) :\n");
		printf("||\t");
        scanned = scanf_s("%f", &MinDia);
        scanf_s("%*[^\n]");
        getchar();
    }
    printf("||\t\n");

    data_length += sizeof(SendPacket.AppData.caliper_params_info);

    // Setup Packet Size
    SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData.caliper_params_info);
    iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
    if (iResult == SOCKET_ERROR) {
        printf("||\tsend failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

    memset(recvbuf, 0x0, DEFAULT_BUFLEN);
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    memcpy((void*)&RecvPacket, recvbuf, iResult);
    if (RecvPacket.PacketInfo.PacketID != SetCaliperInfoResponse)
        printf("||\tSetCaliperInfo error\n");
    printf("||\t\n");

    for (j = 0; j < ArmNB; j++) {
        UserResp = 'n';
        printf("||\t=================================\n");
        printf("||\tPut the  ARM number %d in MAX diameter\n", j + 1);
        printf("||\t=================================\n");
        printf("||\tEnter 'y' when done !\n");
        while (UserResp != 'y') {
			printf("||\t");
            scanf_s("%c", &UserResp, 1);
            scanf_s("%*[^\n]");
            getchar();
        }
        // GetCaliperBScanCommand at MAX diameter
        SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
        data_length += sizeof(SendPacket.PacketInfo);
        iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
        if (iResult == SOCKET_ERROR) {
            printf("||\tsend failed with error: %d\n", WSAGetLastError());
            closesocket(ConnectSocket);
            WSACleanup();
            return 1;
        }
        memset(buffRecv, 0x0, DEFAULT_BUFLEN);
        // Receive until the peer closes the connection
        memset(recvbuf, 0x0, DEFAULT_BUFLEN);
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        memcpy((void*)&RecvPacket, recvbuf, iResult);
        if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
            printf("||\tGetCaliperBScan error\n");
        CalDataArmMax[j] = RecvPacket.AppData.caliper_bscan.ArmsData[j];
        printf("||\t\n");

        UserResp = 'n';
        printf("||\t=================================\n");
        printf("||\tPut the ARM number %d in the EXT1 diameter\n", j + 1);
        printf("||\t=================================\n");
        printf("||\tEnter 'y' when done !\n");
        while (UserResp != 'y') {
			printf("||\t");
            scanf_s("%c", &UserResp, 1);
            scanf_s("%*[^\n]");
            getchar();
        }

        // GetCaliperBScanCommand at EXT1 diameter
        SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
        data_length += sizeof(SendPacket.PacketInfo);
        iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
        if (iResult == SOCKET_ERROR) {
            printf("||\tsend failed with error: %d\n", WSAGetLastError());
            closesocket(ConnectSocket);
            WSACleanup();
            return 1;
        }
        memset(buffRecv, 0x0, DEFAULT_BUFLEN);
        // Receive until the peer closes the connection
        memset(recvbuf, 0x0, DEFAULT_BUFLEN);
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        memcpy((void*)&RecvPacket, recvbuf, iResult);
        if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
            printf("||\tGetCaliperBScan error\n");
        CalDataArmEXT[0][j] = RecvPacket.AppData.caliper_bscan.ArmsData[j];
        printf("||\t\n");

        UserResp = 'n';
        printf("||\t=================================\n");
        printf("||\tPut the ARM number %d in MIN diameter\n", j + 1);
        printf("||\t=================================\n");
        printf("||\tEnter 'y' when done !\n");
        while (UserResp != 'y') {
			printf("||\t");
            scanf_s("%c", &UserResp, 1);
            scanf_s("%*[^\n]");
            getchar();
        }
        // GetCaliperBScanCommand at MAX diameter
        SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
        data_length += sizeof(SendPacket.PacketInfo);
        iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
        if (iResult == SOCKET_ERROR) {
            printf("||\tsend failed with error: %d\n", WSAGetLastError());
            closesocket(ConnectSocket);
            WSACleanup();
            return 1;
        }
        memset(buffRecv, 0x0, DEFAULT_BUFLEN);
        // Receive until the peer closes the connection
        memset(recvbuf, 0x0, DEFAULT_BUFLEN);
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        memcpy((void*)&RecvPacket, recvbuf, iResult);
        if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
            printf("||\tGetCaliperBScan error\n");
        CalDataArmMin[j] = RecvPacket.AppData.caliper_bscan.ArmsData[j];
        printf("||\t\n");
    }

    // SetCaliperCalibrationParamsCommand
    memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
    SendPacket.PacketInfo.PacketID = SetCaliperCalibrationParamsCommand;
    data_length += sizeof(SendPacket.PacketInfo);

    // Setup Header Sub-Info
    SendPacket.PacketSubInfo.FirmwareVer = RecvPacket.PacketSubInfo.FirmwareVer;
    data_length += sizeof(SendPacket.PacketSubInfo);

    // Build the Packet data to send
    //-------------Bits Mask------------//
    SendPacket.AppData.caliper_params_cal.CalMaskBits = 0xcdcd;
    for (i = 0; i < MaxNArms; i++) {
        //-------------MIN/MAX Parameters------------//
        SendPacket.AppData.caliper_params_cal.CalParamMax[i] = CalDataArmMax[i];
        SendPacket.AppData.caliper_params_cal.CalParamMin[i] = CalDataArmMin[i];

        //-------------Extra Parameters------------//
        SendPacket.AppData.caliper_params_cal.ParamsExt1.CalExt[i] = CalDataArmEXT[0][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt2.CalExt[i] = CalDataArmEXT[1][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt3.CalExt[i] = CalDataArmEXT[2][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt4.CalExt[i] = CalDataArmEXT[3][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt5.CalExt[i] = CalDataArmEXT[4][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt6.CalExt[i] = CalDataArmEXT[5][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt7.CalExt[i] = CalDataArmEXT[6][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt8.CalExt[i] = CalDataArmEXT[7][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt9.CalExt[i] = CalDataArmEXT[8][i];
        SendPacket.AppData.caliper_params_cal.ParamsExt10.CalExt[i] = CalDataArmEXT[9][i];
    }

    //-------------MIN/MAX Diameter------------//
    SendPacket.AppData.caliper_params_cal.DiaMax = MaxDia;
    SendPacket.AppData.caliper_params_cal.DiaMin = MinDia;
    SendPacket.AppData.caliper_params_cal.ParamsExt1.DiaExt = ETX1Dia;
    SendPacket.AppData.caliper_params_cal.ParamsExt2.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt3.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt4.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt5.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt6.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt7.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt8.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt9.DiaExt = 0;
    SendPacket.AppData.caliper_params_cal.ParamsExt10.DiaExt = 0;

    data_length += sizeof(SendPacket.AppData.caliper_params_cal);

    // Setup Packet Size
    SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData.caliper_params_cal);
    iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
    if (iResult == SOCKET_ERROR) {
        printf("||\tsend failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

    memset(recvbuf, 0x0, DEFAULT_BUFLEN);
    iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
    memcpy((void*)&RecvPacket, recvbuf, iResult);
    if (RecvPacket.PacketInfo.PacketID != SetCaliperCalibrationParamsResponse)
        printf("||\tSetCaliperCalibrationParams error\n");
    printf("||\t\n");
	while (1)
	{
		UserResp = 'g';
		while ((UserResp != 'y') && (UserResp != 'n')) {
			printf("||\tDo you want to recalibrate some arm ?\n");
			printf("||\tEnter 'y' for yes and 'n' for no\n");
			printf("||\t");
			scanf_s("%c", &UserResp, 1);
			scanf_s("%*[^\n]");
			getchar();
		}
		if (UserResp == 'y')
		{
			printf("||\tWhich arm would you like to recalibrate ?\n");
			printf("||\t");
			scanned = scanf_s("%d", &ArmNBInt);
			scanf_s("%*[^\n]");
			getchar();
			while ((scanned != 1) || (ArmNBInt < 1) || (ArmNBInt > ArmNB))
			{
				printf("||\t\tIncorrect number\n");
				printf("||\tPlease enter the number of the arm\n");
				printf("||\t");
				scanned = scanf_s("%d", &ArmNBInt);
				scanf_s("%*[^\n]");
				getchar();
			}

			UserResp = 'n';
			printf("||\t=================================\n");
			printf("||\tPut the  ARM number %d in MAX diameter\n", ArmNBInt);
			printf("||\t=================================\n");
			printf("||\tEnter 'y' when done !\n");
			while (UserResp != 'y')
			{
				printf("||\t");
				scanf_s("%c", &UserResp, 1);
				scanf_s("%*[^\n]");
				getchar();
			}
			// GetCaliperBScanCommand at MAX diameter
			SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
			data_length += sizeof(SendPacket.PacketInfo);
			iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
			if (iResult == SOCKET_ERROR) {
				printf("||\tsend failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			memset(buffRecv, 0x0, DEFAULT_BUFLEN);
			// Receive until the peer closes the connection
			memset(recvbuf, 0x0, DEFAULT_BUFLEN);
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			memcpy((void*)&RecvPacket, recvbuf, iResult);
			if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
				printf("||\tGetCaliperBScan error\n");
			CalDataArmMax[ArmNBInt - 1] = RecvPacket.AppData.caliper_bscan.ArmsData[ArmNBInt - 1];
			printf("||\t\n");

			UserResp = 'n';
			printf("||\t=================================\n");
			printf("||\tPut the ARM number %d in the EXT1 diameter\n", ArmNBInt);
			printf("||\t=================================\n");
			printf("||\tEnter 'y' when done !\n");
			while (UserResp != 'y') {
				printf("||\t");
				scanf_s("%c", &UserResp, 1);
				scanf_s("%*[^\n]");
				getchar();
			}

			// GetCaliperBScanCommand at EXT1 diameter
			SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
			data_length += sizeof(SendPacket.PacketInfo);
			iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
			if (iResult == SOCKET_ERROR) {
				printf("||\tsend failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			memset(buffRecv, 0x0, DEFAULT_BUFLEN);
			// Receive until the peer closes the connection
			memset(recvbuf, 0x0, DEFAULT_BUFLEN);
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			memcpy((void*)&RecvPacket, recvbuf, iResult);
			if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
				printf("||\tGetCaliperBScan error\n");
			CalDataArmEXT[0][ArmNBInt - 1] = RecvPacket.AppData.caliper_bscan.ArmsData[ArmNBInt - 1];
			printf("||\t\n");

			UserResp = 'n';
			printf("||\t=================================\n");
			printf("||\tPut the ARM number %d in MIN diameter\n", ArmNBInt);
			printf("||\t=================================\n");
			printf("||\tEnter 'y' when done !\n");
			while (UserResp != 'y') {
				printf("||\t");
				scanf_s("%c", &UserResp, 1);
				scanf_s("%*[^\n]");
				getchar();
			}
			// GetCaliperBScanCommand at MAX diameter
			SendPacket.PacketInfo.PacketID = GetCaliperBScanCommand;
			data_length += sizeof(SendPacket.PacketInfo);
			iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
			if (iResult == SOCKET_ERROR) {
				printf("||\tsend failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			memset(buffRecv, 0x0, DEFAULT_BUFLEN);
			// Receive until the peer closes the connection
			memset(recvbuf, 0x0, DEFAULT_BUFLEN);
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			memcpy((void*)&RecvPacket, recvbuf, iResult);
			if (RecvPacket.PacketInfo.PacketID != GetCaliperBScanResponse)
				printf("||\tGetCaliperBScan error\n");
			CalDataArmMin[ArmNBInt - 1] = RecvPacket.AppData.caliper_bscan.ArmsData[ArmNBInt - 1];
			printf("||\t\n");

			// SetCaliperCalibrationParamsCommand
			memset((void*)&SendPacket, 0x0, sizeof(&SendPacket));
			SendPacket.PacketInfo.PacketID = SetCaliperCalibrationParamsCommand;
			data_length += sizeof(SendPacket.PacketInfo);

			// Setup Header Sub-Info
			SendPacket.PacketSubInfo.FirmwareVer = RecvPacket.PacketSubInfo.FirmwareVer;
			data_length += sizeof(SendPacket.PacketSubInfo);

			// Build the Packet data to send
			//-------------Bits Mask------------//
			SendPacket.AppData.caliper_params_cal.CalMaskBits = 0xcdcd;
			for (i = 0; i < MaxNArms; i++) {
				//-------------MIN/MAX Parameters------------//
				SendPacket.AppData.caliper_params_cal.CalParamMax[i] = CalDataArmMax[i];
				SendPacket.AppData.caliper_params_cal.CalParamMin[i] = CalDataArmMin[i];

				//-------------Extra Parameters------------//
				SendPacket.AppData.caliper_params_cal.ParamsExt1.CalExt[i] = CalDataArmEXT[0][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt2.CalExt[i] = CalDataArmEXT[1][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt3.CalExt[i] = CalDataArmEXT[2][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt4.CalExt[i] = CalDataArmEXT[3][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt5.CalExt[i] = CalDataArmEXT[4][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt6.CalExt[i] = CalDataArmEXT[5][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt7.CalExt[i] = CalDataArmEXT[6][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt8.CalExt[i] = CalDataArmEXT[7][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt9.CalExt[i] = CalDataArmEXT[8][i];
				SendPacket.AppData.caliper_params_cal.ParamsExt10.CalExt[i] = CalDataArmEXT[9][i];
			}

			//-------------MIN/MAX Diameter------------//
			SendPacket.AppData.caliper_params_cal.DiaMax = MaxDia;
			SendPacket.AppData.caliper_params_cal.DiaMin = MinDia;
			SendPacket.AppData.caliper_params_cal.ParamsExt1.DiaExt = ETX1Dia;
			SendPacket.AppData.caliper_params_cal.ParamsExt2.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt3.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt4.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt5.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt6.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt7.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt8.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt9.DiaExt = 0;
			SendPacket.AppData.caliper_params_cal.ParamsExt10.DiaExt = 0;

			data_length += sizeof(SendPacket.AppData.caliper_params_cal);

			// Setup Packet Size
			SendPacket.PacketInfo.DataLength = sizeof(SendPacket.AppData.caliper_params_cal);
			iResult = send(ConnectSocket, (char*)&SendPacket, data_length, 0);
			if (iResult == SOCKET_ERROR) {
				printf("||\tsend failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}

			memset(recvbuf, 0x0, DEFAULT_BUFLEN);
			iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			memcpy((void*)&RecvPacket, recvbuf, iResult);
			if (RecvPacket.PacketInfo.PacketID != SetCaliperCalibrationParamsResponse)
				printf("||\tSetCaliperCalibrationParams error\n");
			printf("||\t\n");

		}
		else
		{
			printf("||\t\n||\t======================================\n||\t\n");
			printf("||\t\tCalibration done !\n||\t\n");
			printf("||\t======================================\n||\t\n");

			// shutdown the connection since no more data will be sent
			iResult = shutdown(ConnectSocket, SD_SEND);
			if (iResult == SOCKET_ERROR) {
				printf("||\tshutdown failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			// cleanup
			closesocket(ConnectSocket);
			WSACleanup();

			return 0;
		}
	}
}
